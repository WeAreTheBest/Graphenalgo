/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author john
 */
class Ortnebar implements Comparable<Ortnebar>{
Integer key, val;

    public Ortnebar(int key, int val) {
        this.key = key;
        this.val = val;
    }

    public Integer getKey() {
        return key;
    }

    public int getVal() {
        return val;
    }

    @Override
    public int compareTo(Ortnebar o) {
    return key.compareTo(o.getKey());
    }
    
    
}
