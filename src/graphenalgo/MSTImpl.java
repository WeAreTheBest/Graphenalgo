
import java.util.PriorityQueue;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Minimale Spannbäume.
 *
 * @author john
 */
class MSTImpl implements MST {

    int[] pi;
    double[] val;

    /**
     * Minimalen Spannbaum des Graphen g mit dem Algorithmus von Prim mit
     * Startknoten s berechnen. Der Graph muss ungerichtet sein, d. h. jede
     * Kante muss in beiden Richtungen mit dem gleichen Gewicht vorhanden sein.
     */
    @Override
    public void compute(WeightedGraph g, int s) {

        /**
         * G: Graph VG: Knotenmenge von G w: Gewichtsfunktion für Kantenlänge r:
         * Startknoten (r ∈ VG) Q: Prioritätswarteschlange π[u]: Elternknoten
         * von Knoten u im Spannbaum Adj[u]: Adjazenzliste von u (alle
         * Nachbarknoten) wert[u]: Abstand von u zum entstehenden Spannbaum
         */
        PriorityQueue<Integer> q = new PriorityQueue<>();
//Q \leftarrow VG   //Initialisierung
        for (int i = 0; i < g.size() - 1; i++) {
            q.add(i);
        }
        /**
         * 02 für alle u ∈ Q 03 wert[u] \leftarrow ∞ 04 π[u] \leftarrow 0
         */
        val=new double[q.size()+1];
        pi=new int[q.size()+1];
        for (int u = 0; u < g.size() - 1; u++) {
            val[u] = Integer.MAX_VALUE;
            pi[u] = 0;
        }
        val[s] = 0;

//05  wert[r] \leftarrow 0
        val[s] = 0;
//06  solange Q ≠ \varnothing
        while (!q.isEmpty()) {

//07      u \leftarrow extract_min(Q)
            int u = q.poll();
//08      für alle v ∈ Adj[u]
            for (int i = 0; i < g.degree(u) - 1; i++) {
//09          wenn v ∈ Q und w(u,v) < wert[v]
                if (g.equals(g.succ(u, i)) && g.weight(u, i) < val[g.succ(u, i)]) {
//10              dann π[v] \leftarrow u
                    pi[g.succ(u, i)] = u;
//11                  wert[v] \leftarrow w(u,v)    
                    val[u] = g.weight(u, i);
                }
            }
        }
    }

    /**
     * Vom Algorithmus ermittelte Information: Vorgängerknoten des Knotens v im
     * Spannbaum (bzw. NIL, wenn es keinen Vorgänger gibt)
     *
     * @param v knoten
     * @return einen vorgängerknoten
     */
    @Override
    public int parent(int v) {
        return (pi[v] == Integer.MIN_VALUE ? NIL : pi[v]);
    }

}
