
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author john
 */
public class DFSImpl implements DFS {

    private int[] d, f, col, pi;
    private boolean circ = false;
    static final int GRAY = 1;
    static final int WHITE = 0;
    static final int BLACK = 2;
    private int time = 0;

    /**
     * Tiefensuche im Graphen g durchführen. Die Knoten werden in der
     * Reihenfolge 0 bis g.size() - 1 durchlaufen.
     */
    private void search(Graph g, List<Integer> l) {
      //  System.out.println("search");
        int size = g.size();
        d = new int[size];
        pi = new int[size];
        f = new int[size];
        col = new int[size];
        for (int i : f) {
            i = Integer.MIN_VALUE;
        }
        for (int i : d) {
            i = Integer.MIN_VALUE;
        }
        for (int i : pi) {
            i = Integer.MIN_VALUE;
        }
        for (int v : l) {
            if (col[v] == WHITE) {
                visit(v, g);
            }
        }

    }

    private void visit(int u, Graph g) {
        this.col[u] = GRAY;
        d[u] = ++time;

        for (int i = 0; i < g.degree(u); i++) {
            int curr = g.succ(u, i);
            if (col[curr] == WHITE) {
                pi[curr] = u;
                visit(curr, g);
            }
            if (col[curr] == GRAY) {
                circ = true;
              // System.err.println("circel found");
            }
        }
        col[u] = BLACK;
        f[u] = ++time;
    }

    @Override
    /**
     * Tiefensuche im Graphen g durchführen. Die Knoten werden in der
     * Reihenfolge d.sorted(0) bis d.sorted(g.size() - 1) durchlaufen.
     */
    public void search(Graph g, DFS d) {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < g.size() - 1; i++) {
            list.add(d.sorted(i));
        }
        search(g, list);
    }

    @Override
    /**
     * Topologische Sortierung des Graphen g durchführen. Resultatwert true,
     * wenn dies möglich ist, false, wenn der Graph einen Zyklus enthält.
     */
    public boolean sort(Graph g) {
        search(g);
        return !circ;
    }

    @Override
    /**
     * Von den Algorithmen ermittelte Information: Entdeckungs- bzw
     * Abschlusszeit des Knotens v, jeweils zwischen 1 und 2 * g.size().
     */
    public int detect(int v) {
      //  System.out.println("detect"+v);
        return d[v];
    }

    /**
     * Von den Algorithmen ermittelte Information: Entdeckungs- bzw
     * Abschlusszeit des Knotens v, jeweils zwischen 1 und 2 * g.size().
     */
    @Override
    public int finish(int v) {
        return f[v];
    }

    /**
     * // Für i von 0 bis g.size() - 1 liefert sorted(i) die Knoten // des
     * Graphen g sortiert nach absteigenden Abschlusszeiten. // (Das heißt:
     * sorted(0) ist der Knoten mit der größten  Abschlusszeit,
     * sorted(g.size() - 1) der mit der kleinsten.)
     *
     * @param i
     * @return
     */
    @Override
    public int sorted(int i) {
      //  System.out.println("sorted"+i);
        List<Ortnebar> liste = new ArrayList<>();
      // System.out.println("col"+col.length);
        for (int j = 0; j < col.length; j++) {
            liste.add(new Ortnebar(this.f[j], j));
        }
        Collections.sort(liste);
        Collections.reverse(liste);
        if (liste.size() > i) {
            return liste.get(i).getVal();
        } else {
            return Integer.MIN_VALUE;
        }

    }

    @Override
    /**
     * Von den Algorithmen ermittelte Information: Entdeckungs- bzw
     * Abschlusszeit des Knotens v, jeweils zwischen 1 und 2 * g.size().
     */
    public void search(Graph g) {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < g.size() - 1; i++) {
            list.add(i);
        }
        this.search(g, list);
    }

}
