/*
 * Graphen
 */

// Gerichteter Graph.
// (Ein ungerichteter Graph wird als gerichteter Graph repräsentiert,
// beim jede Kante in beiden Richtungen vorhanden ist.)
interface Graph {
    /**
     * Fügt knoten hinzu
     */
  //  void addNode();
    /**
     * füge eine kante hinzu
     * @param u ursprung
     * @param v ziel;
     */
   // void addEdge(int u, int v);
    // Anzahl Knoten.
    // Die Knoten werden durch Nummern zwischen 0 einschließlich und
    // size() ausschließlich repräsentiert, das heißt:
    // Alle Parameter von Methoden dieser und anderer Schnittstellen,
    // die Knoten bezeichnen (z. B. v), müssen Werte in diesem Bereich
    // besitzen.
    // Methoden, die Knoten als Resultat liefern (z. B. succ), müssen
    // Werte in diesem Bereich liefern.
    int size ();

    // Grad des Knotens v, d. h. Anzahl seiner ausgehenden Kanten
    // bzw. direkten Nachfolger.
    int degree (int v);

    // i-ter direkter Nachfolger des Knotens v.
    // i muss zwischen 0 einschließlich und degree(v) ausschließlich
    // liegen.
    int succ (int v, int i);

    // Transponierter Graph, d. h. ein neuer Graph mit denselben Knoten
    // wie der aktuelle Graph, der für jede Kante (u, v) des aktuellen
    // Graphen die entgegengesetzte Kante (v, u) enthält.
    Graph transpose ();
}

// Gerichteter gewichteter Graph.
// (Ein ungerichteter gewichteter Graph wird als gerichteter gewichteter
// Graph repräsentiert, beim jede Kante in beiden Richtungen mit dem
// gleichen Gewicht vorhanden ist.
interface WeightedGraph extends Graph {
   //  void addEdge (int u, int v, Double w);
    // Gewicht der Kante von v zu seinem i-ten direkten Nachfolger.
    // i muss im selben Bereich wie bei der Methode succ liegen.
    double weight (int v, int i);

    // Transponierter Graph, d. h. ein neuer gewichteter Graph mit
    // denselben Knoten wie der aktuelle Graph, der für jede Kante
    // (u, v) des aktuellen Graphen mit Gewicht w die entgegengesetzte
    // Kante (v, u) mit dem gleichen Gewicht w enthält.
    @Override
    WeightedGraph transpose ();
}

// Gerichteter Graph mit beschrifteten Knoten.
interface LabeledGraph extends Graph {
    // Knoten mit Beschriftung l hinzufügen.
    // Als Resultatwert erhält man die Nummer des Knotens
    // (d. h. der Reihe nach 0, 1, 2, ...).
    int addNode (String l);

    // Kante von Knoten u nach Knoten v hinzufügen
    // (d. h. anschließend gilt: succ(u, degree(u) - 1) == v).
    void addEdge (int u, int v);

    // Beschriftung des Knotens v.
    String label (int v);
}

// Gerichteter gewichteter Graph mit beschrifteten Knoten.
interface LabeledWeightedGraph extends WeightedGraph, LabeledGraph {
    // Kante von Knoten u nach Knoten v mit Gewicht w hinzufügen.
    void addEdge (int u, int v, double w);

    // Kante von Knoten u nach Knoten v mit Gewicht 1 hinzufügen.
    @Override
    void addEdge (int u, int v);
}

/*
 * Algorithmen auf ungewichteten Graphen
 */

// Breitensuche.
interface BFS {
    // Breitensuche im Graphen g mit Startknoten s durchführen.
    void search (Graph g, int s);

    // Vom Algorithmus ermittelte Information:

    // Abstand des Knotens v zum Startknoten s der Suche
    // (bzw. INF, wenn v von s aus nicht erreichbar ist).
    int INF = -1;
    int distance (int v);

    // Vorgängerknoten von v auf dem Pfad von s nach v
    // (bzw. NIL, wenn es keinen Vorgänger gibt).
    int NIL = -1;
    int parent (int v);
}

// Tiefensuche.
interface DFS {
    // Tiefensuche im Graphen g durchführen.
    // Die Knoten werden in der Reihenfolge 0 bis g.size() - 1
    // durchlaufen.
    void search (Graph g);

    // Tiefensuche im Graphen g durchführen.
    // Die Knoten werden in der Reihenfolge d.sorted(0) bis
    // d.sorted(g.size() - 1) durchlaufen.
    void search (Graph g, DFS d);

    // Topologische Sortierung des Graphen g durchführen.
    // Resultatwert true, wenn dies möglich ist,
    // false, wenn der Graph einen Zyklus enthält.
    boolean sort (Graph g);

    // Von den Algorithmen ermittelte Information:

    // Entdeckungs- bzw. Abschlusszeit des Knotens v,
    // jeweils zwischen 1 und 2 * g.size().
    int detect (int v);
    int finish (int v);

    // Für i von 0 bis g.size() - 1 liefert sorted(i) die Knoten
    // des Graphen g sortiert nach absteigenden Abschlusszeiten.
    // (Das heißt: sorted(0) ist der Knoten mit der größten
    // Abschlusszeit, sorted(g.size() - 1) der mit der kleinsten.)
    int sorted (int i);
}

// Starke Zusammenhangskomponenten.
interface SCC {
    /**
     * Starke Zusammenhangskomponenten des Graphen g bestimmen.
    */
     void compute (Graph g);

    // Vom Algorithmus ermittelte Information:

    // Eindeutige Nummer der Zusammenhangskomponente,
    // zu der der Knoten v gehört.
    // (Das heißt: component(u) muss genau dann gleich component(v)
    // sein, wenn u und v zur gleichen Zusammenhangskomponente gehören.
    // Abgesehen davon, können die Nummern beliebig sein.)
    int component (int v);
}

/*
 * Algorithmen auf gewichteten Graphen
 */

// Minimale Spannbäume.
interface MST {
    // Minimalen Spannbaum des Graphen g mit dem Algorithmus von Prim
    // mit Startknoten s berechnen.
    // Der Graph muss ungerichtet sein, d. h. jede Kante muss
    // in beiden Richtungen mit dem gleichen Gewicht vorhanden sein.
    void compute (WeightedGraph g, int s);

    // Vom Algorithmus ermittelte Information:

    // Vorgängerknoten des Knotens v im Spannbaum
    // (bzw. NIL, wenn es keinen Vorgänger gibt).
    int NIL = -1;
    int parent (int v);
}

// Kürzeste Wege.
interface SP {
    // Algorithmus von Bellman-Ford auf dem Graphen g mit Startknoten s
    // ausführen.
    // Resultatwert true genau dann, wenn es im Graphen keinen vom
    // Startknoten aus erreichbaren Zyklus mit negativem Gewicht gibt.
    boolean bellmanFord (WeightedGraph g, int s);

    // Algorithmus von Dijkstra auf dem Graphen g mit Startknoten s
    // ausführen.
    void dijkstra (WeightedGraph g, int s);

    // Von den Algorithmen ermittelte Information:

    // Abstand des Knotens v zum Startknoten s (ggf. INF).
    double INF = Double.POSITIVE_INFINITY;
    double distance (int v);

    // Vorgängerknoten des Knotens v auf dem kürzesten Pfad zum
    // Startknoten (bzw. NIL, wenn es keinen Vorgänger gibt).
    int NIL = -1;
    int parent (int v);
}
