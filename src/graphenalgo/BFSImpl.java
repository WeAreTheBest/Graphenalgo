
import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Breitensuche.
 *
 * @author john
 */
class BFSImpl implements BFS {

    public BFSImpl() {
    }
    private int[] d, pi, col;

    @Override
    /**
     * Breitensuche im Graphen g mit Startknoten s durchführen.
     */
    public void search(Graph g, int s) {
        int size = g.size();
        d = new int[size];
        pi = new int[size];
        col = new int[size];
        for (int i =0; i< pi.length;i++) {
            pi[i] = NIL;
        }
       for (int i =0; i< d.length;i++) {
            d[i] = Integer.MIN_VALUE;
        }

        LinkedList<Integer> lis = new LinkedList<>();
        lis.add(s);
         
        while (lis.size()>0) {
            int curr=lis.removeFirst();
            
             System.out.print(g.degree(curr));
            for (int i = 0; i < g.degree(curr); i++) {
                  System.out.print(".");
                if (col[g.succ(curr, i)] == 0) {
                    int knot = g.succ(curr, i);
                    col[knot] = 1;
                    d[knot] = d[curr] + 1;
                    pi[knot] = curr;
                    lis.add(knot);

                }
                col[curr] = 2;

            }

        }
        pi[s]=Integer.MIN_VALUE;
        System.out.println("bfs finished");

    }

    @Override
    /**
     * Vom Algorithmus ermittelte Information: Abstand des Knotens v zum
     * Startknoten s der Suche (bzw. INF, wenn v von s aus nicht erreichbar
     * ist).
     */
    public int distance(int v) {
        return (d[v]);
    }

    @Override
    /**
     * Vorgängerknoten von v auf dem Pfad von s nach v (bzw. NIL, wenn es keinen
     * Vorgänger gibt).
     */
    public int parent(int v) {
        return pi[v];
    }

}
