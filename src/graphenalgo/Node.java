
import java.util.ArrayList;
import java.util.HashMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author john
 */
class Node {

    String label = "";
    ArrayList<Edge> edges = new ArrayList<>();

    Node(String l) {
        label = l;
    }

    public void addEdge(Node n, Double w) {
        edges.add(new Edge(n, w));
    }

    public String getLabel() {
        return label;
    }

    public Double getWeight(int n) {
        return edges.get(n).getWeight();
    }

    public Node getSucc(int n) {
        return edges.get(n).getNode();
    }

    int getDegree() {
        return edges.size();
    }

}
