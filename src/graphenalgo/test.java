import java.io.*;

// Testprogramm für Graphalgorithmen.
class testest {
    // Der zugrundeliegende Graph, der von readGraph erzeugt wird.
    private static LabeledWeightedGraph graph;

    // Graphbeschreibung von System.in lesen und interpretieren.
    // Zeilen mit einer Spalte label (z. B. "s")
    // definieren einen Knoten mit dieser Beschriftung.
    // Zeilen mit zwei bzw. drei Spalten u, v und ggf. w
    // (z. B. "3 5" oder "4 2 7.0")
    // definieren eine gerichtete Kante vom Knoten u zum Knoten v
    // (jeweils gezählt ab 0), ggf. mit Gewicht w.
    // Der erzeugte Graph wird in der Variablen graph gespeichert.
    private static void readGraph () {
         
	// Leeren Graphen erzeugen.
	graph = new LabeledWeightedGraphImpl();
        //dfs
        /*
        graph.addNode("u");
         graph.addNode("v");
         graph.addNode("w");
         graph.addNode("x");
         graph.addNode("y");
         graph.addNode("z");
         graph.addEdge(0, 1);
         graph.addEdge(0, 3);
         graph.addEdge(1, 4);
         graph.addEdge(2, 5);
         graph.addEdge(2, 4);
         graph.addEdge(1, 3);
         graph.addEdge(4, 3);
         graph.addEdge(5, 5);
         
        */
        //breitensuche
        /*
        graph.addNode("v");
         graph.addNode("r");
         graph.addNode("s");
         graph.addNode("w");
         graph.addNode("t");
         graph.addNode("u");
         graph.addNode("x");
         graph.addNode("y");
         graph.addEdge(0, 1);
         graph.addEdge(1, 0);
         graph.addEdge(2, 1);
         graph.addEdge(1, 2);
         graph.addEdge(2, 3);
         graph.addEdge(3, 2);
         graph.addEdge(4, 3);
         graph.addEdge(3, 4);
         graph.addEdge(4, 5);
         graph.addEdge(5, 4);
         graph.addEdge(6, 5);
         graph.addEdge(5, 6);
         graph.addEdge(6, 3);
         graph.addEdge(3, 6);
         graph.addEdge(6, 7);
         graph.addEdge(7, 6);
         graph.addEdge(5, 7);
         graph.addEdge(7, 5);
        */
        //ssc
         graph.addNode("a");
         graph.addNode("b");
         graph.addNode("c");
         graph.addNode("d");
         graph.addNode("e");
         graph.addNode("f");
         graph.addNode("g");
         graph.addNode("h");
         graph.addEdge(0, 1);
         graph.addEdge(1, 2);
         graph.addEdge(1, 4);
         graph.addEdge(1, 5);
         graph.addEdge(2, 3);
         graph.addEdge(2, 6);
         graph.addEdge(3, 2);
         graph.addEdge(3, 7);
         graph.addEdge(4, 0);
         graph.addEdge(4, 5);
         graph.addEdge(5, 6);
         graph.addEdge(6, 5);
         graph.addEdge(6, 7);
         graph.addEdge(7, 7);
         
        /*
        //Example dijkstra
        graph.addNode("s");
         graph.addNode("t");
         graph.addNode("y");
         graph.addNode("x");
         graph.addNode("z");
         
         graph.addEdge(0, 1,10);
         graph.addEdge(0, 2,5);
         graph.addEdge(1, 2,2);
         graph.addEdge(2, 1,3);
         
         graph.addEdge(1, 3,1);
         graph.addEdge(2, 3,9);
         graph.addEdge(3, 4,4);
         graph.addEdge(4, 3,6);
         graph.addEdge(4, 0,7);
         graph.addEdge(2, 4,2);
        */
        
        //bell
        /*
        graph.addNode("s");
         graph.addNode("t");
         graph.addNode("y");
         graph.addNode("x");
         graph.addNode("z");
         
         graph.addEdge(0, 1,6);
         graph.addEdge(0, 2,7);
         graph.addEdge(1, 2,8);
         graph.addEdge(3, 1,-2);
         
         graph.addEdge(1, 3,5);
         graph.addEdge(2, 3,-3);
         graph.addEdge(1, 4,-4);
         graph.addEdge(4, 3,7);
         graph.addEdge(4, 0,2);
         graph.addEdge(2, 4,9);
         */
         return;
	// Standardeingabe-Strom System.in als InputStreamReader
	// und diesen als BufferedReader r verpacken.
	/*BufferedReader r
	    = new BufferedReader(new InputStreamReader(System.in));

	// Eingabe zeilenweise lesen und verarbeiten.
	// readLine kann eine Ausnahme des Typs IOException werfen.
	// Am Ende der Eingabe wird null geliefert.
	try {
	    String line;

            while ((line = r.readLine()) != null) {
                System.out.println("r"+line+":");

                // Zeile in Spalten zerlegen,
		// die durch Leerzeichen getrennt sind.
		String [] cols = line.split(" ");

		if(line.equals("")||line.equals("\n")||line.equals(" ")){
                    return;
                }
                // Knoten bzw. Kante zum Graphen hinzufügen.
		if (cols.length == 1) {
		    // Knoten.
		    graph.addNode(cols[0]);
		}
		if(cols.length == 2||cols.length == 3) {
		    // Kante.
		    int u = Integer.parseInt(cols[0]);
		    int v = Integer.parseInt(cols[1]);
		    double w = cols.length == 2 ?
				  1 : Double.parseDouble(cols[2]);
		    graph.addEdge(u, v, w);
		}
                
	    }
	}
	catch (IOException e) {
	    e.printStackTrace(System.out);
	    return;
	}
*/
    }

    // name und value ausgeben.
    // Wenn name gleich "parent" ist, wird die Knotennummer value
    // durch die zugehörige Beschriftung (bzw. ein Minuszeichen)
    // ersetzt.
    private static <T> void print (String name, T value) {
	System.out.print(" " + name + "=");
	if (name.equals("parent")) {
	    int v = (Integer)value;
	    System.out.print(v >= 0 ? graph.label(v) : "-");
	}
	else {
	    System.out.print(value);
	}
    }

    // Beschriftung des Knotens v sowie name1, value1, name2 und value2
    // ausgeben (name2 und value2 nur, wenn sie nicht null sind).
    private static <T1, T2>
    void print (int v, String name1, T1 value1, String name2, T2 value2) {
	System.out.print(graph.label(v) + ":");
	print(name1, value1);
	if (name2 != null) print(name2, value2);
	System.out.println();
    }

    // Beschriftung des Knotens v sowie name und value ausgeben.
    private static <T> void print (int v, String name, T value) {
	print(v, name, value, null, null);
    }

    // Hauptprogramm.
    // Aufruf mit einem der folgenden Kommandozeilenargumente:
    // bfs -> breadth first search
    // dfs -> depth first search
    // sort -> topological sort
    // scc -> strongly connected components
    // mst -> minimum spanning tree
    // bell -> Bellman-Ford
    // dijk -> Dijkstra
    // Als zweites Argument kann optional die Nummer des Startknotens
    // (gezählt ab 0) angegeben werden (Standardwert 0).
    public static void main (String [] args) {
	// Kommandozeilenargumente.
	String algo = args[0];
	int s = args.length >= 2 ? Integer.parseInt(args[1]) : 0;

	// Graph graph anhand der eingegebenen Beschreibung konstruieren.
	readGraph();
       
        
	int n = graph.size();

	// Gewünschten Algorithmus ausführen
	// und die von ihm ermittelte Information ausgeben.
	if (algo.equals("bfs")) {
	    BFS a = new BFSImpl();
	    a.search(graph, 2);
         
	    for (int v = 0; v < n; v++) {
		print(v, "distance", a.distance(v), "parent", a.parent(v));
	    }
	}
	else if (algo.equals("dfs") || algo.equals("sort")) {
            System.out.print("df");
	    DFS a = new DFSImpl();
            
	    if (algo.charAt(0) == 'd') {
            System.out.print("  : ");
		a.search(graph);
	    }
	    else {
                 System.out.print("-");
		if (!a.sort(graph)) {
		    System.out.println("cycle");
		    return;
		}
                
	    }
             System.out.print("+");
	    for (int i = 0; i < n; i++) {
                System.out.print("s");
		int v = a.sorted(i);
		print(v, "detect", a.detect(v), "finish", a.finish(v));
	    }
	}
	else if (algo.equals("scc")) {
	    SCC a = new SCCImpl();
	    a.compute(graph);
	    for (int v = 0; v < n; v++) {
		print(v, "component", a.component(v));
	    }
	}
	else if (algo.equals("mst")) {
	    MST a = new MSTImpl();
	    a.compute(graph, s);
	    for (int v = 0; v < n; v++) {
		print(v, "parent", a.parent(v));
	    }
	}
	else if (algo.equals("bell") || algo.equals("dijk")) {
	    SP a = new SPImpl();
	    if (algo.charAt(0) == 'b') {
		if (!a.bellmanFord(graph, s)) {
		    System.out.println("negative cycle");
		    return;
		}
	    }
	    else {
		a.dijkstra(graph, s);
	    }
	    for (int v = 0; v < n; v++) {
		print(v, "distance", a.distance(v), "parent", a.parent(v));
	    }
	}
	else {
	    System.out.println("unknown algorithm: " + algo);
	    return;
	}
    }
}
