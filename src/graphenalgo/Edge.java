/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author john
 */
public class Edge {

    Node n;
    Double w;

    Edge(Node n, Double w) {
        this.n = n;
        this.w = w;
    }

    Double getWeight() {
        return w;
    }

    /**
     * gitb den Knoten Zurück
     */
    Node getNode() {
        return this.n;
    }

}
