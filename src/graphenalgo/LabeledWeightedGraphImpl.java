
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author john
 */
public class LabeledWeightedGraphImpl implements LabeledWeightedGraph {

    private ArrayList<Node> nodes = new ArrayList<>();

    @Override
    /**
     * Knoten mit Beschriftung l hinzufügen. Als Resultatwert erhält man die
     * Nummer des Knotens (d. h. der Reihe nach 0, 1, 2, ...).
     */
    public int addNode(String l) {
        Node n = new Node(l);
        nodes.add(n);
        return nodes.indexOf(n);
    }

    @Override
    /**
     * Kante von Knoten u nach Knoten v hinzufügen (d h anschließend gilt:
     * succ(u, degree(u) - 1) == v).
     */
    public void addEdge(int u, int v) {
        nodes.get(u).addEdge(nodes.get(v), (Double) 0.0);
    }

    @Override
    /**
     * Beschriftung des Knotens v.
     */
    public String label(int v) {
        return nodes.get(v).getLabel();
    }

    /**
     * Gewicht der Kante von v zu seinem i-ten direkten Nachfolger. i muss im
     * selben Bereich wie bei der Methode succ liegen.
     *
     * @param v nummer des Knoten
     * @param i nummer der kante.
     * @return
     */
    @Override
    public double weight(int v, int i) {
        return (nodes.get(v).getWeight(i));
    }

    /**
     * // Transponierter Graph, d. h. ein neuer gewichteter Graph mit //
     * denselben Knoten wie der aktuelle Graph, der für jede Kante // (u, v) des
     * aktuellen Graphen mit Gewicht w die entgegengesetzte // Kante (v, u) mit
     * dem gleichen Gewicht w enthält.
     *
     * @return den neuen Graph
     */
    @Override
    public WeightedGraph transpose() {
        LabeledWeightedGraphImpl a = new LabeledWeightedGraphImpl();
        for (Node node : nodes) {
            a.addNode("");
        }
        for (int i = 0; i < nodes.size(); i++) {
            for (int j = 0; j < nodes.get(i).getDegree(); j++) {

                a.addEdge(nodes.indexOf(nodes.get(i).getSucc(j)), i, nodes.get(i).getWeight(j));

            }
        }
        return a;

    }

    /**
     * // Anzahl Knoten. // Die Knoten werden durch Nummern zwischen 0
     * einschließlich und // size() ausschließlich repräsentiert, das heißt: //
     * Alle Parameter von Methoden dieser und anderer Schnittstellen, // die
     * Knoten bezeichnen (z. B. v), müssen Werte in diesem Bereich // besitzen.
     * // Methoden, die Knoten als Resultat liefern (z. B. succ), müssen //
     * Werte in diesem Bereich liefern.
     */
    @Override
    public int size() {
        return nodes.size();
    }

    /**
     * Grad des Knotens v, d. h. Anzahl seiner ausgehenden Kanten bzw. direkten
     * Nachfolger.
     *
     * @param v knotenidex
     * @return nachfolger
     */
    @Override
    public int degree(int v) {
        return nodes.get(v).getDegree();

    }

    /**
     * // i-ter direkter Nachfolger des Knotens v. // i muss zwischen 0
     * einschließlich und degree(v) ausschließlich // liegen.
     *
     * @param v der knoten
     * @param i der wivielte Nachfolger
     * @return index des Nachfolgers
     */
    @Override
    public int succ(int v, int i) {
        return nodes.indexOf(nodes.get(v).getSucc(i));
    }

    /**
     *  // Kante von Knoten u nach Knoten v mit Gewicht w hinzufügen.
     *
     * @param u ursprungsknoten
     * @param v zielknoten
     * @param w gewicht Der Kante
     */
    @Override
    public void addEdge(int u, int v, double w) {
        nodes.get(u).addEdge(nodes.get(v), w);
        //System.out.print("added Edge");
    }

}
