/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Starke Zusammenhangskomponenten.
 *
 * @author john
 */
class SCCImpl implements SCC {

    DFSImpl s, st;
    Graph g;

    @Override
    public void compute(Graph g) {
        this.g = g;
        s = new DFSImpl();
        s.search(g);
        st = new DFSImpl();
        Graph gt = g.transpose();
        st.search(gt, s);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    /**
     * Vom Algorithmus ermittelte Information: Eindeutige Nummer der
     * Zusammenhangskomponente, zu der der Knoten v gehört. (Das heißt:
     * component(u) muss genau dann gleich component(v) sein, wenn u und v zur
     * gleichen Zusammenhangskomponente gehören. Abgesehen davon, können die
     * Nummern beliebig sein.)
     */
    public int component(int v) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        // 1 Berechne mittels einer Tiefensuche in G die
        //Endzeiten f [u] f¨ur alle Knoten u ∈ V
        int count = 0;
        DFS a = new DFSImpl();
        a.search(g);
        // 2 Berechne G T
        // 3 F¨uhre in G T
        Graph gt = g.transpose();

        DFS b = new DFSImpl();
        //    eine Tiefensuche durch, wobei die Knoten
        //  mit absteigendem f [u] (von Zeile 1) ausgew¨ahlt werden
        b.search(gt, a);
        //4 Gib die Knoten jedes Baums des in Zeile 3 berechneten
        //    Depth-First-Walds als Zusammenhangkomponente aus
        while (b.sorted(count) > Integer.MIN_VALUE) {
            count++;
        }
        return count;
    }

}
