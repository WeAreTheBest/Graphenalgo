
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author john
 */
class SPImpl implements SP {

    int[] pi;
    double[] d;
    WeightedGraph g;

    public SPImpl() {
    }

    @Override
    /*
    * Algorithmus von Bellman-Ford auf dem Graphen g mit Startknoten s
    * ausführen.
    * Resultatwert true genau dann, wenn es im Graphen keinen vom
    * Startknoten aus erreichbaren Zyklus mit negativem Gewicht gibt.
     */
    public boolean bellmanFord(WeightedGraph g, int s) {
        this.g = g;
        //1 InitializeSingleSource(G,s)
        InitializeSingleSource(g, s);
        //2 for i := 1 to |V| − 1 do
        for (int j = 0; j < g.size(); j++) {
            for (int u = 0; u < g.size(); u++) {
                for (int i = 0; i < g.degree(u); i++) {
                    //3 for jede Kante (u, v) ∈ E do
                    relax(u, g.succ(u, i));
                }
            }
        }
        System.out.println("test");
        //5 for jede Kante (u, v) ∈ E do
        for (int u = 0; u < g.size(); u++) {
            for (int i = 0; i < g.degree(u); i++) {
                int succ = g.succ(u, i);
                //6 if d[v] > d[u] + w(u, v) then
                System.err.println("u"+succ+"|"+u+":"+d[succ]+" > "+d[u]+" + "+g.weight(u, i));
                if (d[succ] > d[u] + g.weight(u, i)){
                //7 return false
                System.out.println("return false");
                return false;
                }
            }
        }
        return true;

    }

    @Override
    /**
     * Algorithmus von Dijkstra auf dem Graphen g mit Startknoten s ausführen.
     */
    public void dijkstra(WeightedGraph g, int s) {
        this.g=g;
        // 1 InitializeSingleSource(G,s)
        InitializeSingleSource(g, s);
        //2 Erstelle eine leere Min-Priority-Queue Q
        BinHeap<Integer, Integer> q = new BinHeap<Integer, Integer>();
        ArrayList<BinHeap.Entry<Integer, Integer>> a = new ArrayList<>();
        //3 F¨uge die Knoten v ∈ V mit Schl¨ussel d[v] in Q ein
        for (int v = 0; v < g.size(); v++) {
            a.add(q.insert((int) d[v], v));
        }
        //4 while Q ist nicht leer do
       
        while (!q.isEmpty()) {

        //5 u := ExtractMin(Q)
            int u = q.extractMin().val();

        //6 for jeden Knoten v ∈ Adj[u] do
            for (int v = 0; v < g.size(); v++) {
                //7 if Relax(u, v) then
                if (relax(u, v)) {
                    System.out.println("relaxed" + v);
                    q.decreaseKey(a.get(v), (int) d[v]);
                    //8 DecreaseKey(Q, v, d[v])

                }
            }

        }

    }

    @Override
    /**
     * Von den Algorithmen ermittelte Information: Abstand des Knotens v zum
     * Startknoten s (ggf. INF).
     */
    public double distance(int v) {
        return d[v];
    }

    @Override
    /**
     * Vorgängerknoten des Knotens v auf dem kürzesten Pfad zum Startknoten
     * (bzw. NIL, wenn es keinen Vorgänger gibt).
     */
    public int parent(int v) {
        return pi[v];
    }

    private void InitializeSingleSource(Graph G, int s) {
         System.err.println("test"+w(0,1));
         d= new double[G.size()+1];
         pi= new int[G.size()+1];
        //1 for jeden Knoten v ∈ V do
        for (int i = 0; i < G.size(); i++) {
            d[i] = INF;
            //2 d[v] := ∞
            pi[i] = NIL;
            //3 π[v] := NIL
            //4 d[s] := 0
        }
        d[s] = 0;
    }

    /**
     *
     * @param u knoten a
     * @param v knoten b, d[c] kann verringert werden
     */
    private boolean relax(int u, int v) {

        double weight = w(u,v);
        
        //1 if d[v] > d[u] + w(u, v) then
        if (d[v] > d[u] + weight) {
            //2 d[v] := d[u] + w(u, v)
           // System.err.println("relax"+u+":"+v);
            d[v] = d[u] + weight;
            
            //3 π[v] := u
            pi[v] = u;
            //4 return true
            return true;
        }
        //5 else
        else {
          //    System.err.println("nrelax"+u+":"+v);
           
            //6 return false
            return false;
        }
    }
/**
 * das gewicht 
 * @param u knoten u
 * @param v knoten v
 * @return  das gewicht der kannte zwischen u und v
 */
    private double w(int u, int v) {
      //    System.err.println("weight"+u+":"+v);
        double weight = INF;
        for (int i = 0; i < g.degree(u); i++) {
            if (g.succ(u, i) == v){
            weight = g.weight(u, i);
          //  System.err.println("w:"+u+":"+v+"="+weight);
             return weight;
        }
        
    }
        return INF;
    }
}
